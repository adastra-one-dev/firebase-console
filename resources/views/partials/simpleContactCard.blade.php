<div class="flex w-full border border-gray-200 rounded p-2 bg-white">
	<img src="https://api.adorable.io/avatars/32/vlastimil.klik+avatar@gmail.com" class="rounded-full w-10 h-10 mr-2"/>
	<div class="text-sm">
		<p class="text-gray-800">Wolfgang Amadeus Mozart</p>
		<p class="text-gray-500">Aug 19, 2019</p>
	</div>
</div>
