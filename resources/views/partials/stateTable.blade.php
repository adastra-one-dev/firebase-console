<div class="mt-2 rounded">
	<div class="bg-white w-full p-3 rounded-t">
		Overview
	</div>
	<div class="flex flex-wrap w-full bg-gray-100">
		<div class="flex flex-wrap w-1/4 p-3 h-32 border-r border-b content-between">
			<div class="w-full text-sm uppercase">State
				<small class="block text-gray-500 lowercase">Lorem ipsum</small>
			</div>
			<div class="w-full text-xl">
				<div class="text-red-500 text-right">Disabled</div>
			</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 border-r border-b content-between">
			<div class="w-full text-sm uppercase">State
				<small class="block text-gray-500 lowercase">Lorem ipsum</small>
			</div>
			<div class="w-full text-xl">
				<div class="text-green-500 text-right">Active</div>
			</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 border-r border-b content-between">
			<div class="w-full text-sm uppercase">State
				<small class="block text-gray-500 lowercase">Lorem ipsum</small>
			</div>
			<div class="w-full text-xl">
				<div class="text-orange-400 text-right">Unstable</div>
			</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 border-b  content-between">
			<div class="w-full text-sm uppercase">Jobs per hour</div>
			<div class="w-full text-xl text-right">50</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 h-32 border-r content-between">
			<div class="w-full text-sm uppercase">State
				<small class="block text-gray-500 lowercase">Lorem ipsum</small>
			</div>
			<div class="w-full text-xl text-red-500 text-right">Disabled</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 border-r content-between">
			<div class="w-full text-sm uppercase">State
				<small class="block text-gray-500 lowercase">Lorem ipsum</small>
			</div>
			<div class="w-full text-xl">
				<div class="text-green-500 text-right">Active</div>
			</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 border-r content-between">
			<div class="w-full text-sm uppercase">State
				<small class="block text-gray-500 lowercase">Lorem ipsum</small>
			</div>
			<div class="w-full text-xl">
				<div class="text-green-500 text-right">Active</div>
			</div>
		</div>

		<div class="flex flex-wrap w-1/4 p-3 content-between">
			<div class="w-full text-sm uppercase">Jobs per hour</div>
			<div class="w-full text-xl text-right">50</div>
		</div>
	</div>
</div>
