<form role="form">
	<div class="bg-white border border-gray-200 rounded">
		<div class="bg-gray-500 text-white p-2 font-bold rounded-t">
			Delete records
		</div>

		<div class="p-3">
			<div class="">
				<label for="" class="block mb-1 text-gray-600">First id to delete</label>
				<input type="number" class="shadow-inner block border border-gray-200 rounded p-2 w-full" value="1000">
				<small class="text-gray-400">Prvni badsf dsfo ofgsofdig osdfgo</small>
			</div>
			<div class="">
				<label for="" class="block mb-1 text-gray-600">Items to delete</label>
				<input type="number" class="shadow-inner border border-gray-200 rounded p-2 w-full" value="1">
				<small class="text-gray-400">Prvni badsf dsfo ofgsofdig osdfgo</small>
			</div>
		</div>
		<div class="flex justify-end border-t p-3">
			<button class="p-2 px-4 rounded bg-blue-400 hover:bg-blue-600 text-white mr-1">Cancel
			</button>
			<button class="p-2 px-4 rounded bg-red-400 hover:bg-red-600 text-white">Delete</button>
		</div>
	</div>
</form>
