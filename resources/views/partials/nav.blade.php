<nav class="bg-white border-b border-gray-200 mb-2 p-3">
	<div class="container mx-auto">
		<div class="flex justify-between flex-wrap">
			<div class="flex">
				<svg class="fill-current h-6 w-6 mr-1" width="54" height="54" viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg">
					<path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z"/>
				</svg>
				<span class="mr-2">Firebase managment</span>
			</div>

			<div class="block lg:items-center lg:flex lg:w-auto">
				<ul class="text-sm">
					<li class="block lg:inline-block mt-4 lg:mt-0 px-1">
						<a href="#" class="text-gray-700 hover:text-gray-900">Data browser</a>
					</li>
					<li class="block lg:inline-block mt-4 lg:mt-0 px-1">
						<a href="#" class="text-gray-700 hover:text-gray-900">Settings</a>
					</li>
					<li class="block lg:inline-block mt-4 lg:mt-0 px-1">
						<a href="#" class="text-gray-700 hover:text-gray-900">Logout</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
