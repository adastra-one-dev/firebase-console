<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Laravel</title>

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
	</head>

	<body class="bg-gray-300">
		<div id="app">
			<div class="">
				@include('partials.nav')
			</div>

			<main class="container mx-auto">
				<div class="flex mb-2">
					<div class="">
						@includeWhen(false, 'partials.deleteContactsForm')
					</div>
					<div class="">
					</div>
				</div>
				@includeWhen(false, 'partials.simpleContactCard')

				@include('partials.stateTable')
			</main>
		</div>
	</body>

	<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
</html>
