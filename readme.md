## Simple uploader to firebase database.

Settings. Configuration file can be downloaded directly from google developer console. Then add link to this file 
to environment config file .env.
~~~bash
# full path to configuration file  
FIREBASE_CREDENTIALS="online-check-in-firebase.json"
~~~

Commandline
~~~bash
# upload data from file with starting id
php artisan firebase:users:create-from-csv ~/work/adastra.one/adam-data/accepted.csv --start-id=5000

# delete n users from starting id
php artisan firebase:users:delete 3000 20
~~~

Columns description in uploading file:

* 0 .. First Name,
* 1 .. Last Name,
* 2 .. Email,
* 3 .. Organization,
* 4 .. Owner,
* 5 .. Status ,
* 6 .. Time,
* 7 .. Food Restrictions,,

Columns **First Name** and **Last Name** will be joined to one field name.
