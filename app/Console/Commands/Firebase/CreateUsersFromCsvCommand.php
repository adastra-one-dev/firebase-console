<?php

namespace App\Console\Commands\Firebase;

use Illuminate\Console\Command;
use Kreait\Firebase\Database;

class CreateUsersFromCsvCommand extends Command
{

	private const CSV_DELIMITER = ',';

	private const ID_SUFFIX = 'Adastra Open House';

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'firebase:users:create-from-csv 
		{filename : file to upload} 
		{--start-id=1000 : start id to upload}
		{--empty-lines=0 : create enpty lines on end of list}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Upload users from the csv file';

	/** @var Database */
	private $database;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Database $database)
	{
		parent::__construct();

		$this->database = $database;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$filename   = $this->argument('filename');
		$userId     = (int) $this->option('start-id');
		$emptyLines = (int) $this->option('empty-lines');

		$this->line(sprintf('Uploading users from csv file "%s" with first id #%d.', $filename, $userId));

		$lines = $this->getLines($filename);

		$handler = fopen($filename, 'rb');

		$bar = $this->output->createProgressBar($lines);

		// 0 .. First Name,
		// 1 .. Last Name,
		// 2 .. Email,
		// 3 .. Organization,
		// 4 .. Owner,
		// 5 .. Status ,
		// 6 .. Time,
		// 7 .. Food Restrictions,,

		while ($data = fgetcsv($handler, 1024, self::CSV_DELIMITER)) {
			$key  = sprintf('%d %s', $userId, self::ID_SUFFIX);
			$user = [
				'id'         => $userId,
				'name'       => $data[1] . ' ' . $data[0],
				'email'      => '',
				'job_title'  => '',
				'phone'      => '',
				'company'    => $data[2],
				'dietary'    => $data[3],
				'note'       => '',
				'status'     => 'INVAITED',
				'showInHost' => false,

			];
			$this->database->getReference(sprintf('/users/%s', $key))->set($user);

			$userId++;

			$bar->advance();
		}

		$bar->finish();

		if ($emptyLines > 0) {
			$this->line(sprintf('Creating %d empty lines for random incomers.', $emptyLines));
			for ($i = 0; $i < $emptyLines; $i++) {
				$key  = sprintf('%d %s', $userId, self::ID_SUFFIX);
				$user = [
					'id'         => $userId,
					'name'       => 'zzz empty',
					'email'      => '',
					'job_title'  => '',
					'phone'      => '',
					'company'    => '',
					'dietary'    => '',
					'note'       => '',
					'status'     => 'INVAITED',
					'showInHost' => false,

				];
				$this->database->getReference(sprintf('/users/%s', $key))->set($user);
				$userId++;
			}
		}
	}

	private function getLines(string $file): int
	{
		$handle = fopen($file, 'rb');
		$lines  = 0;

		while (! feof($handle)) {
			$lines += substr_count(fread($handle, 8192), "\n");
		}

		fclose($handle);

		return $lines;
	}
}
