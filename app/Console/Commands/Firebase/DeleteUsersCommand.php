<?php

namespace App\Console\Commands\Firebase;

use Illuminate\Console\Command;
use Kreait\Firebase\Database;

class DeleteUsersCommand extends Command
{

	private const ID_SUFFIX = 'Adastra Open House';

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'firebase:users:delete 
		{from : First id to be deleted} 
		{lines : How many users whould be deleted}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete selected users';

	/** @var Database */
	private $database;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Database $database)
	{
		parent::__construct();

		$this->database = $database;
	}

	public function handle()
	{
		$start = (int) $this->argument('from');
		$lines = (int) $this->argument('lines');

		$this->line(sprintf('Deleting %d users from firebase database, first id "%s".', $lines, $start));

		$bar = $this->output->createProgressBar($lines);

		for ($id = $start; $id < $start + $lines; $id++) {
			$key = sprintf('%d %s', $id, self::ID_SUFFIX);

			$this->database->getReference(sprintf('/users/%s', $key))->remove();

			$bar->advance();
		}

		$bar->finish();
	}
}
